<div align="center"><h1>FastAPI Teste 🖥️</h1></div>


### ✒️ Introdução
API para testes do FastAPI.
### 🔌 Instalação da Aplicação
(Executar comando na pasta do Dockerfile)
docker build -t vitrium-pessoas --no-cache .
### 📀 Iniciar Aplicação
docker run -p 8000:8000 vitrium-pessoas
### 🛠️ Ferramentas Utilizadas
Docker
FastAPI
### 🧔 Responsáveis pelo projeto
Sillas Reis
<div align="center"><img width="500" alt="Logo" src="https://automatizai.com.br/wp-content/uploads/2020/07/Automatizai-horizontal.png"></div>