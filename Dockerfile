FROM python:3.9.1-alpine3.12

RUN pip3 install fastapi uvicorn

COPY app api/app
COPY MOCK_DATA.json /api/MOCK_DATA.json

WORKDIR /api

EXPOSE 8000

CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "8000"]