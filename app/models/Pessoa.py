from pydantic import BaseModel
from typing import Optional

class Pessoa(BaseModel):
    cd_pss: int
    tx_nome: str
    tx_mail: str

class PessoaUpdate(BaseModel):
    cd_pss: Optional[int]
    tx_nome: Optional[str]
    tx_mail: Optional[str]
