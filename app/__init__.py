from fastapi import FastAPI
from app.routes import Pessoa


app = FastAPI()
app.include_router(Pessoa.router)