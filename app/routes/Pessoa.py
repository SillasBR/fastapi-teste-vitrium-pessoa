from fastapi import APIRouter
from app.models.Pessoa import Pessoa, PessoaUpdate

import json


router = APIRouter()


def load_json(file_path: str):
    return json.load(open(file_path))

def refresh_json(file_path:str, new_data):
    with open(file_path, 'w') as json_file:
        json.dump(new_data, json_file, indent=2)
    return load_json(file_path)

json_file = './MOCK_DATA.json'
pessoas = load_json(json_file)


### Manipulação de dados
def novo_registro_pessoa(pessoa:Pessoa):
    global pessoas
    pessoas.append({
        'cd_pss':pessoa.cd_pss,
        'tx_nome':pessoa.tx_nome,
        'tx_mail':pessoa.tx_mail
    })
    pessoas = refresh_json(json_file, pessoas)

def selecao_pessoa(cd_pss=None):
    if cd_pss is not None:
        for i in pessoas:
            if i['cd_pss'] == cd_pss:
                return i
    else:
        return pessoas

def excluir_registro_pessoa(cd_pss:int):
    global pessoas

    for i in range(len(pessoas)):
        if pessoas[i]['cd_pss'] == cd_pss:
            del pessoas[i]
            pessoas = refresh_json(json_file, pessoas)
            break

def editar_registro_pessoa(cd_pss, pessoa:PessoaUpdate):
    global pessoas
    
    old_pessoa = selecao_pessoa(cd_pss)

    if old_pessoa:
        updated_pessoa = {key:value for key,value in old_pessoa.items()}

        for key, value in pessoa:
            if value:
                updated_pessoa[key] = value
        
        pessoas[pessoas.index(old_pessoa)] = updated_pessoa
        
        pessoas = refresh_json(json_file, pessoas)


### Endpoints expostos
@router.post('/cad-pessoa/')
async def cad_pessoa(pessoa: Pessoa):
    novo_registro_pessoa(pessoa)

@router.get('/get-all-pessoa')
async def get_all():
    return selecao_pessoa()

@router.get('/get-pessoa/{cd_pss}')
async def get_pessoa(cd_pss: int):
    return selecao_pessoa(cd_pss)

@router.delete('/delete-pessoa/{cd_pss}')
async def delete_pessoa(cd_pss:int):
    excluir_registro_pessoa(cd_pss)

@router.put('/edit-pessoa/{cd_pss}')
async def edit_pessoa(cd_pss:int, pessoa:PessoaUpdate):
    editar_registro_pessoa(cd_pss, pessoa)
    